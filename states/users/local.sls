include:
  - groups.local
  - users

{% if pillar.users_local[0] is defined %}

{% for user in pillar.users_local %}

{{ user.name }}_user:
  user.present:
    - name: {{ user.name }}
    {% if user.fullname is defined %}
    - fullname: {{ user.fullname }}
    {% endif %}

    {% if user.shell is defined %}
    - shell: {{ user.shell }}
    {% endif %}

    {% if user.home is defined %}
    - home: {{ user.home }}
    {% endif %}

    {% if user.gid is defined %}
    - gid: {{ user.gid }}
    {% endif %}

    {% if user.uid is defined %}
    - uid: {{ user.uid }}
    {% endif %}

    {% if user.groups is defined and user.groups[0] is defined %}
    - groups:
      {% for group in user.groups %}
      - {{ group }}
      {% endfor %}
    {% endif %}

    {% if user.optional_groups is defined and user.optional_groups[0] is defined %}
    - optional_groups:
      {% for group in user.optional_groups %}
      - {{ group }}
      {% endfor %}
    {% endif %}

    {% if user.home is defined %}
    - require:
      - file: /home
    {% endif %}


{% if user.home is defined %}

{{ user.name }}_user_homedir:
  file.directory:
    - group: {{ user.name }}
    - makedirs: true
    - name: {{ user.home }}
    - recurse:
      - group
      - user
    - require:
      - user: {{ user.name }}
    - user: {{ user.name }}

{% endif %}

{% endfor %}

{% endif %}
