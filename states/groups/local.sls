include:
  - groups

{% if pillar.groups_local[0] is defined %}

{% for group in pillar.groups_local %}
{{ group.name }}_group:
  group.present:
    - name: {{ group.name }}
    - gid: {{ group.gid }}
{% endfor %}

{% endif %}
