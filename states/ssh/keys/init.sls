include:
  - ssh
  - users.local

{% if pillar.users_local[0] is defined %}

{% for user in pillar.users_local %}

# Add keys
{% for auth_key in user.auth_keys %}
{{ user.name }}_{{ pillar.ssh_keys.present[auth_key] }}:
  ssh_auth:
    - name: {{ pillar.ssh_keys.present[auth_key] }}
    - present
    - user: {{ user.name }}
    - enc: ssh-rsa
    - require:
      - user: {{ user.name }}
{% endfor %}

# Remove absent keys
{% for auth_key in pillar.ssh_keys.absent %}
{{ user.name }}_{{ auth_key }}:
  ssh_auth:
    - name: {{ auth_key }}
    - absent
    - user: {{ user.name }}
    - require:
      - user: {{ user.name }}
{% endfor %}
{% endfor %}

{% endif %}
