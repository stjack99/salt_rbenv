{% from "ssh/map.jinja" import server with context %}

{% if grains['kernel'] == 'Linux' %}
openssh-server:
  pkg:
    - installed
    - name: {{ server.pkg }}
{% endif %}
