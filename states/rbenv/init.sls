{% from "rbenv/map.jinja" import ruby_pkgs with context %}

include:
  - users.local

rbenv-deps:
   {% if ruby_pkgs.required_pkgs[0] != '' %}
   pkg.installed:
      - pkgs:
        {% for required_pkg in ruby_pkgs.required_pkgs %}
        - {{ required_pkg }}
        {% endfor %}
  {% endif %}

#TODO get users_local.home to build this path
/home/{{ pillar.rbenv.user.name }}/.bashrc:
  file.append:
    - template: jinja
    - makedirs: true
    - sources:
      - salt://rbenv/config/bashrc

/home/{{ pillar.rbenv.user.name }}/.profile:
  file.append:
    - template: jinja
    - makedirs: true
    - sources:
      - salt://rbenv/config/bashrc

{{ pillar.rbenv.version.ruby }}:
  rbenv.installed:
    - default: True
    - require:
      - pkg: rbenv-deps
      - file: /home/{{ pillar.rbenv.user.name }}/.bashrc
      - file: /home/{{ pillar.rbenv.user.name }}/.profile
      - user: {{ pillar.rbenv.user.name }}

/usr/local/rbenv:
  file.directory:
    - user: {{ pillar.rbenv.user.name }}
    - recurse:
      - user
    - require:
      - rbenv: {{ pillar.rbenv.version.ruby }}
      - user: {{ pillar.rbenv.user.name }}

/home/{{ pillar.rbenv.user.name }}/.gemrc:
  file.managed:
    - user: {{ pillar.rbenv.user.name }}
    - group: {{ pillar.rbenv.group.name }}
    - makedirs: true
    - source: salt://rbenv/config/dot_gemrc.jinja
    - template: jinja

/home/{{ pillar.rbenv.user.name }}/.bundle/config:
  file.managed:
    - user: {{ pillar.rbenv.user.name }}
    - group: {{ pillar.rbenv.group.name }}
    - makedirs: true
    - mode: 644
    - source: salt://rbenv/config/dot_bundle_config

install_bundler:
  cmd.run:
    - name: "/bin/bash -l -c '/usr/local/rbenv/shims/gem install bundler'"
    - unless: "/bin/bash -l -c '/usr/local/rbenv/shims/gem list | grep bundler'"
    - user: {{ ruby_pkgs.user }}
    - group: {{ ruby_pkgs.group }}
    - require:
      - file: /home/{{ pillar.rbenv.user.name }}/.bashrc
      - file: /home/{{ pillar.rbenv.user.name }}/.profile
      - file: /home/{{ pillar.rbenv.user.name }}/.gemrc
      - rbenv: {{ pillar.rbenv.version.ruby }}

rbenv_rehash:
  cmd.run:
    - name: /bin/bash -l -c '/usr/local/rbenv/bin/rbenv rehash'
    - user: {{ ruby_pkgs.user }}
    - require:
      - cmd: install_bundler
