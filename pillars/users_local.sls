users_local:
  - name: consul
    fullname: Consul User
    shell: /bin/bash
    home: /var/tmp/consul
    gid: 469
    uid: 469
    optional_groups:
    auth_keys:
      - ci-user
      - person1
      - person2

{% if grains.host.startswith('app1') %}
  - name: app1_user
    fullname: User for App1
    shell: /bin/bash
    home: /home/app1_user
    gid: 8088
    uid: 8088
    auth_keys:
      - ci-user
      - person1
      - person2

{% if grains.host.startswith('app2') %}
  - name: app2_user
    fullname: User for App2
    shell: /bin/bash
    home: /home/app2_user
    gid: 8089
    uid: 8089
    auth_keys:
      - ci-user
      - person2
      - person3
{% endif %}
