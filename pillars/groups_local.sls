groups_local:
  - name: consul
    gid: 469

{% if grains.host.startswith('app1') %}
  - name: app1_user
    gid: 8088
{% endif %}

{% if grains.host.startswith('app2') %}
  - name: app2_user
    gid: 8089
{% endif %}
