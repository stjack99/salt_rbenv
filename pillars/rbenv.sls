rbenv:

{% if grains.host.startswith('app1') %}
  group:
    name: app1_user
  user:
    name: app1_user
  version:
    gems: 2.1.0
    ruby: 2.1.6

{% elif grains.host.startswith('app2') %}
  group:
    name: app2_user
  user:
    name: app2-user
  version:
    gems: 1.9.1
    ruby: 1.9.3p484

{% else %}
# Can set default here if needed
{% endif %}
