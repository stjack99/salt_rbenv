# RBENV Salt States & Pillars

A collection of states and pillars I've written to manage an easily deployed RBENV ruby environment.

---

## Pillars

### groups_local

Set 0+ groups. Requires group name and gid. Can use whatever matching rules you need; I use hostname. Goes hand-in-hand with users_local.

### rbenv

Set only 1 rbenv ruby versions to install. Requires a rbenv user, group, ruby version and gems version to be set. User and group should match users_local and groups_local, unless you manage them outside of salt.

### ssh_keys
Provides a lookup table of key names to keys, used to the ssh.keys state to add keys to the users authorized_keys list.

Keys added to absent will be removed from all managed users from users_local.

### users_local

Set 0+ users. Requires name. Defining 'groups' and 'optional_groups' has had mixed results, depending on slat/os related user management bugs. Use any matching you need, I use hostname. Goes hand-in-hand with groups_local.

---

## States

### groups

Manage whatever is needed inside of init.sls. Groups.local will add all groups defined from the groups_local pillar.

### rbenv

Installs global rbenv and ruby version defined in rbenv pillar. Installs bundler and various OS dependencies. Sets user/group permissions for rbenv. Configures user environment to work the rbenv. Adds gem and bundle configuration to fix common issues.

### ssh

Install / setup ssh server. Ssh.keys manages keys defined in ssh_keys added to authorized_keys for users defined in users_local.

### users

Manage whatever is needed inside of init.sls. Users.local will add all users defined from the users_local pillar.
